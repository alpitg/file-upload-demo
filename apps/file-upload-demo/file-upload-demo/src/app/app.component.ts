import { Component } from '@angular/core';

@Component({
  selector: 'global-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'apps-file-upload-demo';
}
